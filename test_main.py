from main import all_users, register_user, login_user
from pytest import fixture
import os

filename = "users.csv"


def test_all_users():
    expected = ([], 0)
    list_of_all_users = all_users(filename)
    #assert expected == list_of_all_users

def test_user_registered():
    expected = [{'id': 1, 'nome': 'jose', 'email': 'jose@hotmail.com', 'password': '1234'}]
    user_registered = register_user(filename, **{'nome': 'jose', 'email': "jose@hotmail.com", "password": '1234'})  
    #assert expected == user_registered

def test_login_required():
    user_registered = register_user(filename, **{'nome': 'jose', 'email': "jose@hotmail.com", "password": '1234'})  
    user = login_user("jose@hotmail.com", "1234")
    expected = "Usuário autenticado corretamente!"
    assert user == expected

    user = login_user("jose@hotmail.com", "11234")
    expected = "Usuário ou senha não autenticado corretamente"
    assert user == expected
