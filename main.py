import csv, os
fieldnames = ["id", "nome", "email", "password"]

def search_character_id_in_csv(filename):
    id_character = 1
    with open(filename) as file:
        reader = csv.DictReader(file)
        for line in reader:
            id_character = int(line['id']) +1
    return id_character 

def create_file(filename):
    with open(filename, 'w') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()

def proced_all_users(filename):
    id_last_user = search_character_id_in_csv(filename)

    with open(filename, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        rows_number = 0
        users = []
        for rows in reader:
            users.append(rows)
            rows_number += 1
        if rows_number == 0:
            return ([], 0)
        return (users, id_last_user) 


def all_users(filename):
    if os.path.exists(filename):
        result = proced_all_users(filename)
        return result
    return ([], 0)

def register_user(filename, **kwargs): 
    have_file = os.path.exists(filename)
    
    if have_file == False:
        create_file(filename)

    user_id = search_character_id_in_csv(filename)

    list_of_users = []

    dictionary_to_write = {
        'id': int(user_id),
        "nome": kwargs.get('nome'),
        "email": kwargs.get('email'),
        "password": kwargs.get('password'),
    }

    with open(filename, 'a') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow(dictionary_to_write)

    list_of_users.append(dictionary_to_write)
    return list_of_users


sucess_login_msg = "Usuário autenticado corretamente!"
error_login_msg = "Usuário ou senha não autenticado corretamente"

def login_required(func): #Função enclausuladora
    def wrapper(email, password): #Função enclausurada
        with open('users.csv', 'r') as csvfile:
            reader = csv.DictReader(csvfile)
            for user in reader:
                if user['email'] == email and user['password'] == password:
                    return func(email, password)
                else:
                    return error_login_msg
    return wrapper

@login_required
def login_user(email, password):
    return sucess_login_msg
